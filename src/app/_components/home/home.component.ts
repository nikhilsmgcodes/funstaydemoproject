import { Component, OnInit } from '@angular/core';
import { DestinationService } from './../../_services/destination/destination.service';
import {Destination} from '../../_models/destination.model';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  destinations: Destination[];
  showModel: boolean = true;
  newDestination: Destination;
  name: any;
  description: any;
  constructor(private destinationService: DestinationService) { }

  ngOnInit() {
    this.getDestinations();
  }

  getDestinations(){
    this.destinationService.getDestinations().then((data: any)=>{
      console.log(data);
      this.destinations = data;
  })
  }

  addDestination(){
    if(this.name == '' || this.description == ''){
      return alert('Please fill Name & Description');
    }
    else{
      this.newDestination = {name:this.name, description:this.description};
      this.destinations.push(this.newDestination);
      this.newDestination = null;
      this.showModel = true;
    }
  }

  deleteDestination(destination){
    this.destinations.forEach((element, index) => {
      if(element == destination){
        this.destinations.splice(index, 1);
      }
    });
  }

  onClickAddNewDestination(){
    this.showModel = false;
  }

}
