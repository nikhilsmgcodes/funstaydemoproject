export class Destination {
  name: string;
  description: string;
}