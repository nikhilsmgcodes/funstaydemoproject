import { Injectable } from '@angular/core';
import {InMemoryDbService} from 'angular-in-memory-web-api'

@Injectable({
  providedIn: 'root'
})
export class DataService implements InMemoryDbService{

constructor() { }

createDb(){

  let  destinations =  [
   {  name:'Destination 1', description: 'Sample Description 1' },
   {  name:'Destination 2', description: 'Sample Description 2' },
   {  name:'Destination 3', description: 'Sample Description 3' },
   {  name:'Destination 4', description: 'Sample Description 4' }
  ];

  return {destinations};

 }
}
