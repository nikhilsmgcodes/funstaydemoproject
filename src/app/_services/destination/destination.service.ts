import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Destination } from 'src/app/_models/destination.model';

@Injectable({
  providedIn: 'root'
})
export class DestinationService {

SERVER_URL: string = "http://localhost:4200/api/";
constructor(private httpClient: HttpClient) { }

public handleError(error: any): Promise<any> {
  console.error('An error occurred', error);
  return Promise.reject(error.message || error);
}

public getDestinations(): Promise<Destination>{ 
  return this.httpClient.get(this.SERVER_URL + 'destinations')
  .toPromise()
      .then(response => response as Destination)
      .catch(this.handleError);
}

}
